# TSIB

## Requirements

1. PageTitle–Genericheaderstylingrequired.
2. SearchBox–GenericsearchaccessingYouTubeAPI.
3. SearchResults–Listthefirst5results,withpaginationtocaptureandlist
remaining results.
4. 5recentsearches–Listthe5recentsearchesperformedinthesession.
5. 5recommendations–Listthe5recommendationstobebasedonmostrecent
search.


## Assessment

* Completeness of the test
* Attention to design details (e.g. padding, leading etc.)
* Responsive behaviour
* Your usage of semantic HTML5
* OO Javascript or Javascript frameworks (Vue.js, Anguar.js, React.js etc.)
* Use of CSS pre-processors
* Use of workflow automators (e.g. Gulp, Grunt, Webpack etc.)


## Google youtube api

API key: AIzaSyDMEgyECScLdhmGq-YcNFFQ2HFawNstPCw
API key is usable from within the browser with any referrer.
API reference/console: https://developers.google.com/apis- explorer/#p/youtube/v3/


## Deployment 

1. Configure and deploy a Docker image and your codebase.
2. Deploy your code in any fashion to the assigned server.
3. Create a git repository and send us the instructions to pull and review.


## Server Details

```
IP: 149.28.175.66 
Username: root 
Password: codetest
```