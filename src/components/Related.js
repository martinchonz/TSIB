let axios = require('axios');
let BASE_URL = 'https://www.googleapis.com/youtube/v3/search';

module.exports = function (options, callback) {
   
    let params = {
        part: 'snippet',
        key: options.apiKey,
        relatedToVideoId: options.id,
        maxResults: (options.items) ? options.items: 5,
        type: 'video'
    };
    
    axios.get(BASE_URL, { params })
        .then(response => {
            if(callback) { callback(response.data.items) }
        })
        .catch(e => console.log(e))
}
